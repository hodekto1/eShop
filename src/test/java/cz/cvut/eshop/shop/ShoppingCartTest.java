package cz.cvut.eshop.shop;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.*;

public class ShoppingCartTest {

    @Test
    public void getCartItems_insertNewAndGet_shouldReturnInsertedOne() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);

        assertEquals(1, shCart.getCartItems().size());

        Iterator<Item> it = shCart.getCartItems().iterator();
        assertEquals(item.getID(), it.next().getID());
        assertFalse(it.hasNext());

    }

    @Test
    public void removeItem_insertAndRemove_itemsSizeShouldBeZero() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);
        assertEquals(1, shCart.getCartItems().size());

        shCart.removeItem(item.getID());
        assertEquals(0, shCart.getCartItems().size());

    }

    @Test
    public void getItemsCount_insertTwoItems_shouldBeTwo() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);
        shCart.addItem(item);

        assertEquals(2, shCart.getItemsCount());
    }

    @Test
    public void getTotalPrice_insertOneItem_shouldBeSameAsItemPrice() {
        ShoppingCart shCart = new ShoppingCart();
        Item item = new StandardItem(1, "bread", 4.50f, "food", 3);
        shCart.addItem(item);

        assertEquals(item.getPrice(), shCart.getTotalPrice(),0.05f);
    }

    @Test
    public void getTotalPrice_insertNone_shouldBeZero() {
        ShoppingCart shCart = new ShoppingCart();

        assertEquals(0f, shCart.getTotalPrice(),0.05f);
    }
}